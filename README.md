# React Calendar Playground

## Description

A calendar playground is built by

- React.js
- Webpack
- Babel
- Yarn
- Sass
- ESLint

### Development

```bash
yarn serve
```

### Production

```bash
yarn build
```
