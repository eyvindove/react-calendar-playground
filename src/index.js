import React from 'react'
import ReactDOM from 'react-dom'

import './index.scss'

import Calendar from './components/Calendar/index.js'
import DatePicker from './components/DatePicker'

const App = () => {
  return (
    <main className="app">
      <section className="section-1">
        <h4>Part 1</h4>
        <Calendar />
      </section>
      <section className="section-2">
        <h4>Part 2</h4>
        <DatePicker />
      </section>
    </main>
  )
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
