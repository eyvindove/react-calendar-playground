import React, { useState, useEffect } from 'react'
import './index.scss'

import Header from '../Header'
import CalendarContent from '../CalendarContent'

export const ContextDisplayInfo = React.createContext()

const Calendar = React.forwardRef((props, ref) => {

  // -- Export Component API

  let selectedDate = null

  React.useImperativeHandle(ref, () => ({
    onSelect () {
      return selectedDate
    }
  }))

  const initialDisplayInfo = {
    today: '',
    type: 'date',
    leapYear: false,
    displayDate: '1970-01-01',
    selectedDate: '1970-01-01',
  }

  // -- state

  const [displayInfo, setDisplayInfo] = useState(initialDisplayInfo)

  const consoleSelectedDate = (date) => {
    console.warn(`The selected date: ${date}`)
  }

  // -- check leap year

  const checkLeapYear = (date) => {
    const currentYear = date ? new Date(date).getFullYear() : new Date().getFullYear()

    return (currentYear % 4 === 0 && currentYear % 100 !== 0) || currentYear % 400 === 0
  }

  const getDateISOString = (date) => {
    const dateObject = date ? new Date(date) : new Date()

    return dateObject.toISOString().slice(0, 10)
  }

  const toggleYearMonth = ({ type, action }) => {
    const date = new Date(displayInfo.displayDate)
    const currentMonth = date.getMonth()
    const currentYear = date.getFullYear()
    let tempDisplayDate = null

    if (type === 'date') {
      tempDisplayDate = action === 'increase'
        ? date.setMonth(currentMonth + 1)
        : date.setMonth(currentMonth - 1)
    } else if (type === 'month') {
      tempDisplayDate = action === 'increase'
        ? date.setFullYear(currentYear + 1)
        : date.setFullYear(currentYear - 1)
    } else {
      tempDisplayDate = action === 'increase'
      ? date.setFullYear(currentYear + 10)
      : date.setFullYear(currentYear - 10)
    }

    setDisplayInfo({
      ...displayInfo,
      leapYear: checkLeapYear(tempDisplayDate),
      displayDate: getDateISOString(tempDisplayDate)
    })
  }

  const toggleType = (type) => {
    setDisplayInfo({
      ...displayInfo,
      type,
    })
  }

  const setDate = ({ type, nextType, date }) => {
    if (type === 'display') {
      setDisplayInfo({
        ...displayInfo,
        displayDate: date,
        type: nextType,
      })
    } else {
      setDisplayInfo({
        ...displayInfo,
        selectedDate: date,
        displayDate: date,
        type: nextType,
      })
      consoleSelectedDate(date)
      selectedDate = date
    }
  }

  useEffect(() => {
    if (displayInfo.today == '') {
      setDisplayInfo({
        ...displayInfo,
        today: getDateISOString(),
        leapYear: checkLeapYear(),
        displayDate: getDateISOString(),
        selectedDate: getDateISOString(),
      })
    }
  })

  return (
    <section className="calendar" onClick={props.calendarOnClick}>
      <ContextDisplayInfo.Provider value={displayInfo}>
        <Header
          toggleYearMonth={toggleYearMonth}
          toggleType={toggleType}
          />
        <CalendarContent
          toggleType={toggleType}
          setDate={setDate}
          />
      </ContextDisplayInfo.Provider>
    </section>
  )
})

export default Calendar
