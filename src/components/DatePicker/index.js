import React, { useState, useRef } from 'react'
import './index.scss'

import Calendar from '../Calendar'

const DatePicker = () => {
  const [date, setDate] = useState('')
  const [isVisible, setIsVisible] = useState(false)

  const inputPlaceholder = '1970-01-01'

  const refCalendar = useRef()

  const setSelectedDate = () => {
    const selectedDate = refCalendar.current.onSelect()

    setDate(selectedDate)
    setIsVisible(!isVisible)
  }

  const toggleVisible = () => {
    setIsVisible(!isVisible)
  }

  const isInputEmpty = date === ''
    ? <div className="date-picker__input__placeholder">{inputPlaceholder}</div>
    : <div className="date-picker__input__label">{date}</div>

  const isCalendarVisible = isVisible
    ? <Calendar ref={refCalendar} calendarOnClick={setSelectedDate} />
    : null

  return (
    <div className="date-picker">
      <div className="date-picker__input" onClick={() => toggleVisible()}>{isInputEmpty}</div>
      {isCalendarVisible}
    </div>
  )
}

export default DatePicker
