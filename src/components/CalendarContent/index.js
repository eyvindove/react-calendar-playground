import React, { useContext, useState } from 'react'
import './index.scss'

import WeekDay from '../WeekDay'

import { ContextDisplayInfo } from '../Calendar'

const CalendarContent = (props) => {
  const displayInfo = useContext(ContextDisplayInfo)

  const monthInfo = [
    { id: '01', label: 'Jan', length: 31 },
    { id: '02', label: 'Feb', length: displayInfo.leapYear ? 29 : 28 },
    { id: '03', label: 'Mar', length: 31 },
    { id: '04', label: 'Apr', length: 30 },
    { id: '05', label: 'May', length: 31 },
    { id: '06', label: 'Jun', length: 30 },
    { id: '07', label: 'Jul', length: 31 },
    { id: '08', label: 'Aug', length: 31 },
    { id: '09', label: 'Sep', length: 30 },
    { id: '10', label: 'Oct', length: 31 },
    { id: '11', label: 'Nov', length: 30 },
    { id: '12', label: 'Dec', length: 31 },
  ]

  // -- Check whether is selected date
  const isSameYear = displayInfo.displayDate.slice(0, 4) === displayInfo.selectedDate.slice(0, 4)
  const isSameMonth = displayInfo.displayDate.slice(0, 8) === displayInfo.selectedDate.slice(0, 8)

  const isSelected = (item) => {
    let result = false

    if (displayInfo.type === 'date') {
      result = isSameMonth && item.type === 'current' && item.label === Number(displayInfo.selectedDate.slice(8, 10))
    } else if (displayInfo.type === 'month') {
      result = isSameYear && item.id === displayInfo.selectedDate.slice(5, 7)
    } else {
      result = item.label === Number(displayInfo.selectedDate.slice(0, 4))
    }

    return result ? 'selected' : ''
  }

  const isSameMonthWithToday = displayInfo.today.slice(0, 8) === displayInfo.displayDate.slice(0, 8)
  
  const isToday = (item) => {
    const isSameDate = item.label === Number(displayInfo.today.slice(8, 10))

    return isSameMonthWithToday && item.type === 'current' && isSameDate ? 'today' : ''
  }

  const setSelectedDate = (dateInfo) => {
    if (displayInfo.type === 'date') {
      props.setDate({
        type: 'selected',
        nextType: 'date',
        date: `${displayInfo.displayDate.slice(0, 7)}-${String(dateInfo.label).padStart(2, '0')}`,
      })
    } else if (displayInfo.type === 'month') {
      props.setDate({
        type: 'display',
        nextType: 'date',
        date: `${displayInfo.displayDate.slice(0, 4)}-${dateInfo.id}-${displayInfo.displayDate.slice(8, 10)}`,
      })
    } else {
      props.setDate({
        type: 'display',
        nextType: 'month',
        date: `${dateInfo.label}-${displayInfo.displayDate.slice(5)}`,
      })
    }
  }

  const monthArray = () => {
    const year = Number(displayInfo.displayDate.slice(0, 4))
    const month = Number(displayInfo.displayDate.slice(5, 7)) - 1
    const firstDay = new Date(year, month, 1).getDay()

    const tempMonthArray = []
    const prevMonth = month - 1 < 0 ? 11 : month - 1

    for (let prev = monthInfo[prevMonth].length; prev > monthInfo[prevMonth].length - firstDay; prev -= 1) {
      tempMonthArray.unshift({
        type: 'prev',
        label: prev,
      })
    }
    for (let current = 0; current < monthInfo[month].length; current += 1) {
      tempMonthArray.push({
        type: 'current',
        label: current + 1,
      })
    }
    for (let next = 0; next < 14; next += 1) {
      tempMonthArray.push({
        type: 'next',
        label: next + 1,
      })
    }

    return tempMonthArray.slice(0, 42)
  }

  // -- Display date

  const displayDateItem = monthArray().map(item => (
    <div
      className={`date__item ${item.type} ${isSelected(item)} ${isToday(item)}`}
      key={`${item.type}-${item.label}`}
      onClick={() => setSelectedDate(item)}
    >
      <div className="date__item__label">{item.label}</div>
    </div>
  ))

  const displayDate = <div className="date">{displayDateItem}</div>

  const displayDefault = (
    <React.Fragment>
      <WeekDay />
      {displayDate}
    </React.Fragment>
  )

  // -- Display month

  const displayMonthItem = monthInfo.map(item => (
    <div
      className={`month__item ${isSelected(item)}`}
      key={item.id}
      onClick={() => setSelectedDate(item)}
    >
      <div className="month__item__label">{item.label}</div>
    </div>
  ))

  const displayMonth = <div className="month">{displayMonthItem}</div>

  // -- Display year

  const displayYearIntervalList = () => {
    const list = []
    const firstYear = Number(displayInfo.displayDate.slice(0, 3) + 0) - 1
    const selectedYear = Number(displayInfo.displayDate.slice(0, 4))

    for (let count = 0; count < 12; count += 1) {
      list.push({
        id: count,
        label: firstYear + count,
        disabled: count === 0 || count === 11 ? 'disabled' : '',
      })
    }

    return list.map(item => (
      <div
        className={`year__item ${item.disabled} ${isSelected(item)}`}
        key={item.id}
        onClick={() => setSelectedDate(item)}
      >
        <div className="year__item__label">{item.label}</div>
      </div>
    ))
  }

  const displayYearInterval = <div className="year">{displayYearIntervalList()}</div>

  const displayContent =
    displayInfo.type === 'date'
      ? displayDefault
      : displayInfo.type === 'month'
        ? displayMonth
        : displayInfo.type === 'year'
          ? displayYearInterval
          : null

  return (
    <div className="calendar-content">
      {displayContent}
    </div>
  )
}

export default CalendarContent
