import React, { useContext } from 'react'
import './index.scss'

import { ContextDisplayInfo } from '../Calendar'

const Header = (props) => {
  const displayInfo = useContext(ContextDisplayInfo)

  const dateObject = new Date(displayInfo.displayDate)
  const monthString = dateObject.toLocaleString('en', { month: 'short' })
  const yearString = dateObject.toLocaleString('en', { year: 'numeric' })
  const yearIntervalString = `${yearString.slice(0, 3)}0-${yearString.slice(0, 3)}9`

  const toggleYearMonth = (action) => {
    props.toggleYearMonth({ type: displayInfo.type, action })
  }

  const toggleType = (type) => {
    props.toggleType(type)
  }

  const displayString = 
    displayInfo.type === 'date'
      ? `${monthString} ${yearString}`
      : displayInfo.type === 'month'
        ? yearString
        : displayInfo.type === 'year'
          ? yearIntervalString
          : null
  
  const displayDatePointerDisabled = displayInfo.type === 'year' ? 'pointer-disabled' : ''

  const clickEvent =
    displayInfo.type === 'date'
      ? () => toggleType('month')
      : displayInfo.type === 'month'
        ? () => toggleType('year')
        : null

  return (
    <div className="header">
      <div className="header__arrow header__arrow--prev" onClick={() => toggleYearMonth('decrease')}></div>
      <div className={`header__display-date ${displayDatePointerDisabled}`} onClick={() => clickEvent()}>{displayString}</div>
      <div className="header__arrow header__arrow--next" onClick={() => toggleYearMonth('increase')}></div>
    </div>
  )
}

export default Header
