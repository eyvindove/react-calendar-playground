import React from 'react'
import './index.scss'

const WeekDay = () => {
  const weekDayList = [
    { id: 'sunday', label: 'Su' },
    { id: 'monday', label: 'Mo' },
    { id: 'tuesday', label: 'Tu' },
    { id: 'wednesday', label: 'We' },
    { id: 'thursday', label: 'Th' },
    { id: 'friday', label: 'Fr' },
    { id: 'saturday', label: 'Sa' },
  ]

  const weekDay = weekDayList.map(item => (
    <div className="weekday__item" key={item.id}>
      <div className="weekday__item__text">{item.label}</div>
    </div>
  ))

  return (
    <div className="weekday">
      {weekDay}
    </div>
  )
}

export default WeekDay
